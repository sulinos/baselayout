#!/bin/sh
#SulinOS is debug distro
#we must enable debug verbose if boot with debug mode
#
if grep "debug" /proc/cmdline &>/dev/null ; then
    #glibc
    #export LD_DEBUG=all #disabled by default. please see LD_DEBUG=help
    export LD_VERBOSE=1
    #mesa
    export LIBGL_DEBUG=verbose
    #gtk-qt
    export G_MESSAGES_DEBUG=all
    export QT_LOGGING_DEBUG=1
    export QML_IMPORT_TRACE=1
fi