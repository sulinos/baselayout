#!/bin/bash
[[ "$PSMODE" == "" ]] && PSMODE="2"
if [[ "$PSMODE" == "2" && "$happy" == "" ]] ; then
    if [[ $UID -eq 0 && -d /proc/1 && "$(stat -c %d:%i /)" != "$(stat -c %d:%i /proc/1/root/. 2>/dev/null)" ]] ; then
       declare -r HOST_TYPE="chroot"
    elif grep docker /proc/1/cgroup -qa &>/dev/null; then
       declare -r HOST_TYPE="docker"
    elif [[ ! "" == "$(LANG=C grep Hypervisor /proc/cpuinfo 2>/dev/null)" ]]; then
       declare -r HOST_TYPE="($(LANG=C grep Hypervisor /proc/cpuinfo  2>/dev/null | head -n 1 | sed 's/^.* //g'))"
    fi

    declare -r txtreset="\[$(tput sgr0)\]"
    declare -r txtbold="\[$(tput bold)\]"
    declare -r txtblack="\[$(tput setaf 0)\]"
    declare -r txtred="\[$(tput setaf 1)\]"
    declare -r txtgreen="\[$(tput setaf 2)\]"
    declare -r txtyellow="\[$(tput setaf 3)\]"
    declare -r txtblue="\[$(tput setaf 4)\]"
    declare -r txtpurple="\[$(tput setaf 5)\]"
    declare -r txtcyan="\[$(tput setaf 6)\]"
    declare -r txtwhite="\[$(tput setaf 7)\]"
    declare -r blink="\[\033[5m\]"
    declare -r sad=':('
    declare -r happy=':)'
    git_path="$(which git 2>/dev/null)"

    set_prompt()
    {
       local last_cmd=$?
       [[ "$-" =~ "x" ]] && local setx="x"
       set +x
       
       [[ "$USER" == "" ]] && USER="unknown"   
       PS1="${txtbold}"
       if [[ $UID == 0 ]]; then
           PS1+="${txtred}${USER}"
       else
       PS1+="${txtyellow}${USER}"
       fi

       PS1+="${txtreset}@${txtbold}${txtblue}${HOSTNAME}:"
       PS1+="${txtcyan}$(basename "$PWD") "
   
       if [[ $last_cmd == 0 ]]; then
          PS1+="${txtgreen}${happy}${txtwhite}"
       else
          PS1+="${txtred}${sad} ${txtwhite}($last_cmd)"
       fi

       if [[ "${git_path}" != "" ]] ; then
          local branch="$(git branch 2>/dev/null | grep '^*'| sed 's/^\* //g')"
          [[ "$branch" == "" ]] || PS1+="${txtpurple} [${branch}]"
       elif [[ "${HOST_TYPE}" != "" ]] ;  then
          PS1+="${txtgreen} (${HOST_TYPE})"
       fi

       PS1+="${txtyellow}${blink} >${txtreset} "
       history -a
       history -r
       [[ "$setx" == "x" ]] && set -x && set -x
    }
fi

if [[ "$PSMODE" == "0" ]] ; then
    export PS1
elif [[ "$PSMODE" == "1" ]] ; then
    PS1="\[\033[1m\]$$|\u@\h \[\033[0;31m\]\W \$ \[\033[00m\]"
elif [[ "$PSMODE" == "2" ]] ; then
    PROMPT_COMMAND='set_prompt'
elif [[ "$PSMODE" == "3" ]] ; then
    PS1="\W \\$"
fi

